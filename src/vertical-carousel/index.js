/**
 * BLOCK: vertical-carousel
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import IntroH2 from "../components/intro-h2"; '../components/intro-h2';
import Testimonial from "../components/testimonial";

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InspectorControls, ColorPalette } = wp.blockEditor;
const { Button, PanelBody, ToggleControl } = wp.components;
const { Fragment } = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-vertical-carousel', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'vertical-carousel - CGB Block' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'vertical-carousel — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		heading: {
			type: 'string',
			source: 'text',
			selector: 'h2',
		},
		description: {
			type: 'array',
			source: 'children',
			selector: 'p.text',
		},
        linkOption: {
		    type: 'boolean',
            default: false,
        },
        linkLabel: {
		    type: 'string',
            default: 'Read More',
        },
        linkUrl: {
		    type: 'string',
        },
		contentArray: {
			type: 'array',
			default: [],
		},
		blockStyle: {
			type: 'object',
		},
		paddingSize: {
			type: 'string',
			default: '20px',
		},
	},
	supports: {
		anchor: true,
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: {
				heading,
                description,
                linkOption,
                linkLabel,
                linkUrl,
				contentArray,
				blockStyle,
			},
			setAttributes,
		} = props;

		const updateAttribute = ( key, value ) => {
			setAttributes( {
				[key]: value,
			} );
		};

		const addContent = ( heading, text ) => {
			const currentContent = [...contentArray];
			currentContent.push( {
				itemHeading: heading,
				itemText: text,
			} );
			setAttributes( { contentArray: currentContent } );
		}

		const removeContent = ( index ) => {
			let confirmDelete = window.confirm( "Do you confirm item removal?" );
			if ( confirmDelete ) {
				const currentContent = [...contentArray];
				currentContent.splice( index, 1 );
				setAttributes( { contentArray: currentContent } );
			} else return false;
		}

		const changeItemHeading = ( itemHeading, index ) => {
			const currentContent = [...contentArray];
			currentContent[index].itemHeading = itemHeading;
			setAttributes( { contentArray: currentContent } );
		}
		const changeItemText = ( itemText, index ) => {
			const currentContent = [...contentArray];
			currentContent[index].itemText = itemText;
			setAttributes( { contentArray: currentContent } );
		}



		let contentItems,
			itemsDisplay;

		if ( contentArray.length ) {
			contentItems = contentArray.map( ( element, index ) => {
				return (
					<Fragment key={ index }>
						<Button
							isDestructive
							onClick={ () => removeContent( index ) }
						>{ __( 'Delete item' ) }</Button>
						<Testimonial
							testimonial={ contentArray[index].testimonial }
						/>
						{/*<RichText*/}
						{/*	tagName="h3"*/}
						{/*	value={ contentArray[index].itemHeading }*/}
						{/*	onChange={ val => changeItemHeading( val, index ) }*/}
						{/*	placeholder="Item heading"*/}
						{/*	keepPlaceholderOnFocus={ true }*/}
						{/*/>*/}
						{/*<RichText*/}
						{/*	tagName="p"*/}
						{/*	value={ contentArray[index].itemText }*/}
						{/*	onChange={ val => changeItemText( val, index ) }*/}
						{/*	placeholder="Item text"*/}
						{/*	keepPlaceholderOnFocus={ true }*/}
						{/*/>*/}
					</Fragment>
				)
			} )

			itemsDisplay = contentItems.map( ( element, index ) => {
				return (
					<div
						className="flex-item"
						key={ index }
					>{ element }
					</div>
				)
			} )
		}

		return [
			<InspectorControls key="1">
				<PanelBody title="Background color">
					<ColorPalette
						onChange={ val => {
							setAttributes( {
								blockStyle: {
									backgroundColor: val,
								},
							} )
						}
						}
					/>
				</PanelBody>
			</InspectorControls>,
			<div key="2" className={ className } style={ blockStyle }>
				<IntroH2
					linkOption={ linkOption }
                    onToggleChange={ () => setAttributes( { linkOption: !linkOption } ) }
                    linkLabel={ linkOption ? linkLabel : '' }
                    linkUrl={ linkOption ? linkUrl : '' }
                    onLinkLabelChange={ val => updateAttribute( 'linkLabel', val ) }
                    onLinkUrlChange={ val => updateAttribute( 'linkUrl', val ) }
                    heading={ heading }
                    onHeadingChange={ val => updateAttribute( 'heading', val ) }
                    text={ description }
                    onTextChange={ val => updateAttribute( 'description', val ) }
				/>
				<div className="flex-container">
					{ itemsDisplay }
					<div className="btn-container">
						<Button
							isPrimary
							onClick={ addContent.bind( this ) }
						>
							{ __( 'Add text item' ) }
						</Button>
					</div>
				</div>
			</div>,
		];


	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			attributes: {
				heading,
                description,
                linkOption,
                linkLabel,
                linkUrl,
				contentArray,
				blockStyle,
			},
		} = props;

		const contentItems = contentArray.map( ( item, index ) => {
			return(
				<div className="vertical-carousel-item wow fadeInUp" key={ index }>
					<div className="vertical-carousel-item-title">
						<RichText.Content
							tagName="h3"
							value={ item.itemHeading }
						/>
					</div>
					<RichText.Content
						tagName="p"
						value={ item.itemText }
					/>
				</div>
			)
		} )

		return (
			<section
				className="p-100"
				style={ blockStyle }
			>
				<div className="container">
					<h2 className="wow fadeInUp">{ heading }</h2>
					<div className="vertical-carousel">
						{ contentItems }
					</div>
				</div>
			</section>
		);
	},
} );
