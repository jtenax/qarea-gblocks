/**
 * BLOCK: cta-row
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

import ButtonWithLink from '../components/btn-with-link';
import ButtonWithAction from '../components/btn-with-action';
import FormDropdownControl from "../components/form-select";


const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	AlignmentToolbar,
	BlockVerticalAlignmentToolbar,
	BlockControls,
	ColorPalette,
	InspectorControls,
	RichText,
} = wp.blockEditor;
const {
	PanelBody,
	PanelRow,
	RadioControl,
} = wp.components;
const { Fragment } = wp.element;


/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-cta-row', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'cta-row - CGB Block' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'call', 'qarea' ),
		__( 'action', 'qarea' ),
		__( 'qarea', 'qarea' ),
	],
	attributes: {
		title: {
			type: 'string',
			source: 'text',
			selector: '.h3',
			default: 'We stress about quality so that you don’t have to.',
		},
		alignment: {
			type: 'string',
			default: 'left',
		},
		blockStyle: {
			type: 'object',
			default: {
				backgroundColor: '#f33',
			},
		},
		buttonStyle: {
			type: 'object',
			default: {
				color: '#f33',
			},
		},
		linkButtonLabel: {
			type: 'string',
			default: 'Download',
		},
		buttonUrl: {
			url: 'string',
		},
		option: {
			type: 'string',
			default: 'form',
		},
		actionButtonLabel: {
			type: 'string',
			default: 'Download'
		},
		contactFormId: {
			type: 'string',
		},
		btnClass: {
			type: 'string',
			default: 'btn-primary-red',
		},
		randomString: {
			type: 'string',
			source: 'attribute',
			selector: 'span.btn',
			attribute: 'data-form',
		},
		modalThanksImage: {
			type: 'object',
		},
		modalThanksHeading: {
			type: 'string',
			default: '',
		},
		modalThanksPar: {
			type: 'string',
			default: 'You can download PDF samples <a href="https://qarea.com/ebook/Complete-Testing-Report.pdf" download target="_blank" rel="noopener noreferrer">here</a>.\n' +
				'A copy has also been sent to your email.',
		},
		modalIntroHeading: {
			type: 'string',
			default: 'Samples of Testing Documentation',
		},
		modalIntroPar: {
			type: 'string',
			default: 'You\'ll be able to download documentation PDF samples immediately. A copy will also be sent to your email address.',
		},
	},
	supports: {
		anchor: true,
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: {
				title,
				alignment,
				blockStyle,
				linkButtonLabel,
				buttonUrl,
				option,
				contactFormId,
				actionButtonLabel,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
				modalIntroHeading,
				modalIntroPar,
			},
			setAttributes,
		} = props;

		const updateAttribute = ( key, value ) => {
			setAttributes( {
				[key]: value,
			} );
		};
		const onRadioSelect = ( option ) => {
			setAttributes({
				option: option,
			});
		};
		const selectAction = ( option ) => {
			if ( option === 'link' ) {
				return (
					<ButtonWithLink
						text={ linkButtonLabel }
						url={ buttonUrl }
						onButtonLabelChange={ val => updateAttribute( 'linkButtonLabel', val ) }
						onURLChange={ val => updateAttribute( 'buttonUrl', val ) }
					/>
				);
			} else {
				return(
					<ButtonWithAction
						text={ actionButtonLabel }
						onButtonLabelChange={ val => updateAttribute( 'actionButtonLabel', val ) }
						formId={ contactFormId }
					/>
				);
			}
		};
		const modalArgs = ( option ) => {
			if ( option === 'form' ) {
				return(
					<div>
						<FormDropdownControl
							// Thank you window
							media={ modalThanksImage }
							setModalImage={ ( media ) => {
								setAttributes({ modalThanksImage: media });
							} }
							removeModalImage={ () => setAttributes({ modalThanksImage: '' }) }
							modalThanksHeading={ modalThanksHeading }
							modalThanksHeadingEdit={ val => updateAttribute( 'modalThanksHeading', val ) }
							modalThanksPar={ modalThanksPar }
							modalThanksParEdit={ val => updateAttribute( 'modalThanksPar', val ) }
							// Form modal
							modalIntroHeading={ modalIntroHeading }
							modalIntroEdit={ val => updateAttribute( 'modalIntroHeading', val ) }
							modalIntroPar={ modalIntroPar }
							modalIntroParEdit={ val => updateAttribute( 'modalIntroPar', val ) }
							// Select parameters
							query={{
								per_page : -1, // set -1 to display ALL
								orderby : 'date',
								order : 'asc',
							}}
							setModalId={ ( randomString, content ) => {
								setAttributes({
									contactFormId: content,
									randomString: randomString + '-' + content,
								});
							} }

						/>
					</div>
				)
			}
		};


		// Creates a <p class='wp-block-cgb-block-cta-row'></p>.
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody title={ __( 'Background color', 'qarea' ) }>
						<PanelRow>
							<ColorPalette // Element Tag for Gutenberg standard colour selector
								onChange={ val => {
									const buttonColor = ( val === 'transparent' ) ? '#f33' : val;
									setAttributes( {
										blockStyle: {
											backgroundColor: val,
										},
										buttonStyle: {
											color: buttonColor,
										},
									} )
									}
								}
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody title={ __( 'Button action' ) }>
						<PanelRow>
							<RadioControl
								selected={ option }
								options={ [
									{
										label: 'Contact form',
										value: 'form',
									},
									{
										label: 'Page link',
										value: 'link',
									},
								] }
								onChange={ onRadioSelect }
							/>
						</PanelRow>
					</PanelBody>
					{ modalArgs( option ) }

				</InspectorControls>
				<div
					className={ className }
					style={ blockStyle }
				>
					<BlockControls>
						<AlignmentToolbar
							value={ alignment }
							onChange={ val => setAttributes( { alignment: val } ) }
						/>
						<BlockVerticalAlignmentToolbar />
					</BlockControls>
					<div className="container">
						<RichText
							tagName="h3"
							className="h3"
							style={{ textAlign: alignment }}
							value={ title }
							onChange={ val => updateAttribute( 'title', val ) }
						/>
						{ selectAction( option ) }
					</div>
				</div>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			className,
			attributes: {
				alignment,
				title,
				blockStyle,
				linkButtonLabel,
				buttonUrl,
				buttonStyle,
				option,
				contactFormId,
				actionButtonLabel,
				randomString,
				btnClass,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
				modalIntroHeading,
				modalIntroPar,
			},
		} = props;

		const selectAction = ( option ) => {
			if ( option === 'link' ) {
				return(
					<ButtonWithLink.View
						btnClass={ btnClass }
						text={ linkButtonLabel }
						url={ buttonUrl }
						style={ buttonStyle }
					/>
				);
			} else {
				return (
					<ButtonWithAction.View
						btnClass={ btnClass }
						text={ actionButtonLabel }
						style={ buttonStyle }
						formId={ contactFormId }
						modalId={ randomString }
						modalIntroHeading={ modalIntroHeading }
						modalIntroPar={ modalIntroPar }
						modalThanksImage={ modalThanksImage }
						modalThanksHeading={ modalThanksHeading }
						modalThanksPar={ modalThanksPar }
					/>
				);
			}
		};

		return (
			<div
				className={ className + ' wow fadeInUp' }
				style={ blockStyle }
			>
				<div className="container">
					<div className="download-section" data-wow-delay="0.3s">
						<RichText.Content
							tagName="h3"
							className="h3"
							style={{ textAlign: alignment }}
							value={ title }
						/>
						{ selectAction( option ) }
					</div>
				</div>
			</div>
		);
	},
} );
