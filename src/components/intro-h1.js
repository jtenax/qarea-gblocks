import React from 'react';
import ImageUpload from "./img-upload";

const { __ } = wp.i18n;
const {
	RichText,
	InspectorControls,
} = wp.blockEditor;
const {
	PanelBody,
	PanelRow,
	ToggleControl,
} = wp.components;
const { Fragment } = wp.element;

const IntroH1 = ( props ) => {

	const selectHeadingType = ( textHeading ) => {
		if ( textHeading ) {
			return (
				<RichText
					tagName="h1"
					value={ props.heading }
					placeholder="Heading..."
					keepPlaceholderOnFocus={ true }
					onChange={ props.onHeadingChange }
				/>
			)
		} else {
			return (
				<ImageUpload
					media={ props.media }
					onImageSelect={ props.setHeadingImage }
					onImageRemove={ props.removeHeadingImage }
				/>
			)
		}
	}

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody title={ __( 'Heading type' ) }>
					<PanelRow>
						<ToggleControl
							label="Text/image background"
							help={ props.headingOption ? 'Text heading' : 'Image heading' }
							checked={ props.headingOption }
							onChange={ props.onToggleChange }
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>
			{ selectHeadingType( props.headingOption ) }
			<RichText
				tagName="p"
				value={ props.text }
				placeholder="Description..."
				keepPlaceholderOnFocus={ true }
				onChange={ props.onTextChange }
			/>
		</Fragment>
	)
}

IntroH1.View = ( props ) => {
	const selectHeadingType = ( textHeading ) => {
		if ( textHeading ) {
			return(
				<RichText.Content
					className="wow fadeInUp without-image"
					tagName="h1"
					value={ props.heading }
				/>
			)
		} else {
			return (
				<h1 classname="wow fadeInUp">
					{ props.heading }
					<img
						src={ props.headingImage ? props.headingImage.url : '' }
						alt=""
					/>
				</h1>
			)
		}
	}
	return (
		<Fragment>
			{ selectHeadingType( props.headingOption ) }
			<RichText.Content
				className="text wow fadeInUp"
				tagName="p"
				data-wow-delay="0.3s"
				value={ props.text }
			/>
		</Fragment>
	);
};

export default IntroH1;
