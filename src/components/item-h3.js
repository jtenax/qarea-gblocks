import React from 'react';

const { __ } = wp.i18n;
const { Button } = wp.components;

const ItemH3 = (props ) => {
	const {
		contentArray,
		changeItemHeading,
		changeItemText,
	} = props;

	let contentItems,
		itemsDisplay;

	if ( contentArray.length ) {
		contentItems = contentArray.map( ( element, index ) => {
			return (
				<Fragment key={ index }>
					<Button
						isSecondary
						onClick={ () => removeContent( index ) }
					>{ __( 'Delete item' ) }</Button>
					<RichText
						tagName="h3"
						value={ contentArray[index].itemHeading }
						onChange={ changeItemHeading }
					/>
					<RichText
						tagName="p"
						value={ contentArray[index].itemText }
						onChange={ val => changeItemText( val, index ) }
					/>
				</Fragment>
			)
		} )

		itemsDisplay = contentItems.map( ( element, index ) => {
			return (
				<div
					className="flex-item"
					key={ index }
				>{ element }
				</div>
			)
		} )
	}
}

export default ItemH3;
