import React from 'react';

const { InspectorControls, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { PanelBody, Button, ResponsiveWrapper } = wp.components;
const { Fragment } = wp.element;
const { withSelect } = wp.data;
const { __ } = wp.i18n;

const ALLOWED_MEDIA_TYPES = [ 'image' ];

const ImageUpload = ( props ) => {
	const { media } = props;

	return (
		<MediaUploadCheck>
			<MediaUpload
				multiple={ false }
				value={ media ? media.id : '' }
				onSelect={ props.onImageSelect }
				allowedTypes={ ALLOWED_MEDIA_TYPES }
				render={ ({ open }) => (
					media ?
						<div>
							<p>
								<img src={ media.url } width={ media.width / 2 } alt=""/>
							</p>
							<p>
								<Button
									onClick={ props.onImageRemove }
									className="button is-small"
								>Remove</Button>
							</p>
						</div> :
						<Button
							onClick={ open }
							className="button"
						>Upload Image</Button>
				) }
			/>
		</MediaUploadCheck>
	);
};

export default ImageUpload;
