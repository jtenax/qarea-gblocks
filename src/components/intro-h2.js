import React from 'react';
import Link from './link';

const { __ } = wp.i18n;
const {
    RichText,
    InspectorControls,
} = wp.blockEditor;
const {
    PanelBody,
    PanelRow,
    ToggleControl,
} = wp.components;
const { Fragment } = wp.element;

const IntroH2 = ( props ) => {
    const showLink = ( linkOption ) => {
        if ( linkOption ) {
            return (
                <Link
                    text={ props.linkLabel }
                    onLinkLabelChange={ props.onLinkLabelChange }
                    url={ props.linkUrl }
                    onLinkUrlChange={ props.onLinkUrlChange }
                />
            )
        }
    }

    return (
        <Fragment>
            <InspectorControls>
                <PanelBody title={ __( 'Link option' ) }>
                    <PanelRow>
                        <ToggleControl
                            label="Show link"
                            help={ props.linkOption ? 'Show link' : 'No link' }
                            checked={ props.linkOption }
                            onChange={ props.onToggleChange }
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
            <RichText
                tagName="h2"
                value={ props.heading }
                placeholder="Heading..."
                keepPlaceholderOnFocus={ true }
                onChange={ props.onHeadingChange }
            />
            <RichText
                tagName="p"
                value={ props.text }
                placeholder="Description..."
                keepPlaceholderOnFocus={ true }
                onChange={ props.onTextChange }
            />
            { showLink( props.linkOption ) }
        </Fragment>
    )
}

IntroH2.View = ( props ) => {
    const showLink = ( linkOption ) => {
        if ( linkOption ) {
            return (
                <a href={ props.linkUrl }>{ props.linkLabel }</a>
            )
        }
    }
    
    return (
        <Fragment>
            <RichText.Content
                className="wow fadeInUp without-image"
                tagName="h1"
                value={ props.heading }
            />
            <RichText.Content
                className="text wow fadeInUp"
                tagName="p"
                data-wow-delay="0.3s"
                value={ props.text }
            />
            { showLink( props.linkOption ) }
        </Fragment>
    );
};

export default IntroH2;
