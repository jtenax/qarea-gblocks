import React from 'react';

const { RichText } = wp.blockEditor;
const { RawHTML, Fragment } = wp.element;

const ButtonWithAction = ( props ) => {
	return (
		<Fragment>
			<RichText
				className="btn"
				value={ props.text }
				placeholder="Button label"
				keepPlaceholderOnFocus={ true }
				onChange={ props.onButtonLabelChange }
			/>
			<RawHTML>{ '[contact-form-7 id="' + props.formId + '"]' }</RawHTML>
		</Fragment>
	);
};

ButtonWithAction.View = ( props ) => {
	return (
		<Fragment>
			<RichText.Content
				className={ 'btn btn-modal-open ' + props.btnClass }
				style={ props.style }
				value={ props.text }
				tagName="span"
				data-form={ props.modalId }
			/>
			<div
				style="display: none;"
				id={ props.modalId }
				className="t-center"
			>
				<h4 className="t-center">{ props.modalIntroHeading }</h4>
				<p>{ props.modalIntroPar }</p>
				<RawHTML>{ '[contact-form-7 id="' + props.formId + '"]' }</RawHTML>
			</div>
			<div
				style="display: none;"
				className="form-modal t-center"
				id={ 'thank-' + props.modalId }
			>
				<img
					src={ props.modalThanksImage ? props.modalThanksImage.url : '' }
					alt="Animated Letter"
					className="img-center mb-50"
				/>
				<h4>{ props.modalThanksHeading }</h4>
				<p><RawHTML>{ props.modalThanksPar }</RawHTML></p>
			</div>
		</Fragment>
	);
};

export default ButtonWithAction;
