import React from 'react';

const {
	RichText,
	URLInputButton,
} = wp.blockEditor;

const Link = ( props ) => {
	return (
		<div>
			<RichText
				value={ props.text }
				placeholder="Link label"
				keepPlaceholderOnFocus={ true }
				onChange={ props.onLinkLabelChange }
			/>
			<URLInputButton
				url={ props.url }
				onChange={ props.onLinkUrlChange }
			/>
		</div>
	);
};

Link.View = ( props ) => {
	return (
		<div>
			<RichText.Content
				value={ props.text }
				tagName="a"
				href={ props.url }
			/>
		</div>
	);
};

export default Link;
