import React from 'react';
import ImageUpload from "./img-upload";

const { __ } = wp.i18n;
const {
	SelectControl,
	Panel,
	PanelBody,
	PanelRow,
	TextControl,
	TextareaControl,
} = wp.components;
const { compose } = wp.compose;
const { withSelect, withDispatch } = wp.data;
const { Fragment } = wp.element;

const FormDropdownControl = compose(
	// withDispatch allows to save the selected post ID into post meta
	withDispatch( function( dispatch, props ) {
		return {
			setMetaValue: function( metaValue ) {
				dispatch( 'core/editor' ).editPost(
					{ meta: { contact_form_id: metaValue } }
				);
			}
		}
	} ),
	// withSelect allows to get posts for our SelectControl and also to get the post meta value
	withSelect( ( select, props ) => {
		return {
			posts: select( 'core' ).getEntityRecords( 'postType', 'wpcf7_contact_form', props.query ),
			metaValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'contact_form_id' ],
		}
	} ) )( ( props ) => {

		// options for SelectControl
		let options = [];

		// if posts found
		if( props.posts ) {
			options.push( { value: 0, label: 'Select something' } );
			props.posts.forEach((post) => { // simple foreach loop
				options.push({value:post.id, label:post.title.rendered});
			});
		} else {
			options.push( { value: 0, label: 'Loading...' } )
		}

		return (
			<Fragment>
				<Panel header="Modal Parameters">
					<PanelBody title={ __( 'Modal form select' ) }>
						<PanelRow>
							<SelectControl
								label='Select a form'
								options={ options }
								onChange={ ( content ) => {
									props.setMetaValue( content );
									let randomString = Math.random().toString(36).substr(2, 7);
									props.setModalId( randomString, content );
								} }
								value={ props.metaValue }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody
						title={ __( 'Modal window intro' ) }
						initialOpen={ false }
					>
						<PanelRow>
							<TextControl
								label="Heading"
								value={ props.modalIntroHeading }
								onChange={ props.modalIntroHeadingEdit }
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label="Paragraph"
								value={ props.modalIntroPar }
								onChange={ props.modalIntroParEdit }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody
						title={ __( 'Thank you window' ) }
						initialOpen={ false }
					>
						<PanelRow>
							<ImageUpload
								media={ props.media }
								onImageSelect={ props.setModalImage }
								onImageRemove={ props.removeModalImage }
							/>
						</PanelRow>
						<PanelRow>
							<TextControl
								label="Heading"
								value={ props.modalThanksHeading }
								onChange={ props.modalThanksHeadingEdit }
							/>
						</PanelRow>
						<PanelRow>
							<TextareaControl
								label="Paragraph"
								value={ props.modalThanksPar }
								onChange={ props.modalThanksParEdit }
							/>
						</PanelRow>
					</PanelBody>
				</Panel>
			</Fragment>
		);
	}
);

export default FormDropdownControl;
