import React from 'react';

const { RichText } = wp.blockEditor;
const { Fragment } = wp.element;

const Testimonial = ( props ) => {
    return (
        <Fragment>
            <RichText
                tagName="p"
                value={ props.testimonial }
            />
        </Fragment>
    )
}

Testimonial.View = ( props ) => {
    return (
        <div className="card-comment">
            
        </div>
    )
}

export default Testimonial;