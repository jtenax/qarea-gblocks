import React from 'react';

const {
	RichText,
	URLInputButton,
} = wp.blockEditor;

const ButtonWithLink = ( props ) => {
	return (
		<div>
			<RichText
				className="btn"
				value={ props.text }
				placeholder="Button label"
				keepPlaceholderOnFocus={ true }
				onChange={ props.onButtonLabelChange }
			/>
			<URLInputButton
				url={ props.url }
				onChange={ props.onURLChange }
			/>
		</div>
	);
};

ButtonWithLink.View = ( props ) => {
	return (
		<div>
			<RichText.Content
				className={ 'btn ' + props.btnClass }
				style={ props.style }
				value={ props.text }
				tagName="a"
				href={ props.url }
			/>
		</div>
	);
};

export default ButtonWithLink;
