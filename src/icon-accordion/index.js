/**
 * BLOCK: icon-accordion
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InspectorControls, ColorPalette } = wp.blockEditor;
const { Button, PanelBody, PanelRow, ToggleControl, } = wp.components;
const { Fragment } = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-icon-accordion', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'icon-accordion - CGB Block' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'icon-accordion — CGB Block', 'qarea' ),
		__( 'accordion', 'qarea' ),
		__( 'qarea', 'qarea' ),
	],
	attributes: {
		iconOption: {
			type: 'boolean',
			default: true,
		},
		contentArray: {
			type: 'array',
			default: [],
		},
		blockStyle: {
			type: 'object',
		},
	},


	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: {
				iconOption,
				contentArray,
				blockStyle,
			},
			setAttributes,
		} = props;

		const updateAttribute = ( key, value ) => {
			setAttributes( {
				[key]: value,
			} );
		};

		const addContent = ( heading, text ) => {
			const currentContent = [...contentArray];
			currentContent.push( {
				itemHeading: heading,
				itemText: text,
			} );
			setAttributes( { contentArray: currentContent } );
		}

		const removeContent = ( index ) => {
			let confirmDelete = window.confirm( "Do you confirm item removal?" );
			if ( confirmDelete ) {
				const currentContent = [...contentArray];
				currentContent.splice( index, 1 );
				setAttributes( { contentArray: currentContent } );
			} else return false;
		}

		const changeItemHeading = ( itemHeading, index ) => {
			const currentContent = [...contentArray];
			currentContent[index].itemHeading = itemHeading;
			setAttributes( { contentArray: currentContent } );
		}
		const changeItemText = ( itemText, index ) => {
			const currentContent = [...contentArray];
			currentContent[index].itemText = itemText;
			setAttributes( { contentArray: currentContent } );
		}


		let contentItems,
			itemsDisplay;

		if ( contentArray.length ) {
			contentItems = contentArray.map( ( element, index ) => {
				return (
					<Fragment key={ index }>
						<Button
							isDestructive
							onClick={ () => removeContent( index ) }
						>{ __( 'Delete item' ) }</Button>
						<RichText
							tagName="h5"
							value={ contentArray[index].itemHeading }
							onChange={ val => changeItemHeading( val, index ) }
							placeholder="Item heading"
							keepPlaceholderOnFocus={ true }
						/>
						<RichText
							tagName="p"
							value={ contentArray[index].itemText }
							onChange={ val => changeItemText( val, index ) }
							placeholder="Item text"
							keepPlaceholderOnFocus={ true }
						/>
					</Fragment>
				)
			} )

			itemsDisplay = contentItems.map( ( element, index ) => {
				return (
					<div
						className="acc-item"
						key={ index }
					>{ element }
					</div>
				)
			} )
		}

		return [
			<InspectorControls key="1">
				<PanelBody title="Background color">
					<ColorPalette
						onChange={ val => {
							setAttributes( {
								blockStyle: {
									backgroundColor: val,
								},
							} )
						}
						}
					/>
				</PanelBody>
				<PanelBody title="Accordion type">
					<ToggleControl
						label={ __( 'Animated icon' ) }
						help={ iconOption ? 'Show icon' : 'No icon' }
						checked={ iconOption }
						onChange={ val => updateAttribute( 'iconOption', val ) }
					/>
				</PanelBody>
			</InspectorControls>,
			<div key="2" className={ className } style={ blockStyle }>
				<div className="acc-container">
					{ itemsDisplay }
					<div className="btn-container">
						<Button
							isPrimary
							onClick={ addContent.bind( this ) }
						>
							{ __( 'Add accordion item' ) }
						</Button>
					</div>
				</div>
			</div>,
		];


	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			attributes: {
				iconOption,
				contentArray,
				blockStyle,
			},
		} = props;

		const contentItems = contentArray.map( ( item, index ) => {
			if ( iconOption ) {
				return (
					<Fragment>
						<h5 className="faq-head">
							<span id={ 'q' + ++index } className="faq-icon"></span> <span className="faq-head-text">{ item.itemHeading }</span>
						</h5>
						<div className="faq-content">
							<RichText.Content
								tagName="p"
								value={ item.itemText }
							/>
						</div>
					</Fragment>
				)
			} else {
				return (
					<Fragment>
						<h5 className="acc-team-head">
							{ item.itemHeading }
						</h5>
						<div className="acc-team-content">
							<RichText.Content
								tagName="p"
								value={ item.itemText }
							/>
						</div>
					</Fragment>
				)
			}
		} )

		return (
			<div className={ ( iconOption ? 'faq' : 'acc-team' ) + ' wow fadeInUp' }>
				{ contentItems }
			</div>
		)

	},
} );
