/**
 * BLOCK: inner-navigation
 *
 * Registering a basic block with Gutenberg.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { RichText } = wp.blockEditor;
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

/**
 * Register: a Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-inner-navigation', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'inner-navigation - CGB Block' ), // Block title.
	icon: 'editor-ul', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'qarea', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'inner-navigation — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'strong',
		},
		navlinks: {
			type: 'array',
			source: 'children',
			selector: '.inner-nav'
		}
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: { title, navlinks },
			setAttributes,
		} = props;
		const onChangeTitle = ( value ) => {
			setAttributes( { title: value } );
		};
		const onChangeNav = ( value ) => {
			setAttributes( { navlinks: value } );
		}
		// Creates a <p class='wp-block-cgb-block-inner-navigation'></p>.
		return (
			<div className={ className }>
				<div className="test">
				<RichText
					tagName="strong"
					placeholder={ __(
						'Write Recipe title…',
						'gutenberg-examples'
					) }
					value={ title }
					onChange={ onChangeTitle }
				/>
				<RichText
					tagName="ul"
					multiline="li"
					placeholder={ __(
						'Write a list of links…',
						'gutenberg-examples'
					) }
					value={ navlinks }
					onChange={ onChangeNav }
					className="inner-nav"
				/>
				</div>
			</div>

		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			className,
			attributes: { title, navlinks },
		} = props;
		return (
			<div className={ className }>
				<div className="test">
					<div className="test-1">
						<RichText.Content tagName="strong" value={ title } />
					</div>
				<div className="test-2">
					<RichText.Content
						tagName="ul"
						className="inner-nav"
						value={ navlinks } />
				</div>

				</div>
			</div>

		);
	},
} );
