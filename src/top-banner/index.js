/**
 * BLOCK: top-banner
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import IntroH1 from '../components/intro-h1';
import ButtonWithAction from '../components/btn-with-action';
import FormDropdownControl from "../components/form-select";
import ImageUpload from '../components/img-upload';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	ColorPalette,
} = wp.blockEditor;
const {
	PanelBody,
	PanelRow,
	ToggleControl
} = wp.components;
const { Fragment } = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-top-banner', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'top-banner - CGB Block' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'top-banner', 'qarea' ),
		__( 'hero', 'qarea' ),
		__( 'qarea', 'qarea' ),
	],
	attributes: {
		headingImage: {
			type: 'object',
		},
		heading: {
			type: 'string',
			source: 'text',
			selector: 'h1',
		},
		description: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
		alignment: {
			type: 'string',
			default: 'left',
		},
		blockStyle: {
			type: 'object',
			default: {
				backgroundColor: '#fafafa',
			},
		},
		actionButtonLabel: {
			type: 'string',
			default: 'Let\'s talk'
		},
		contactFormId: {
			type: 'string',
		},
		btnClass: {
			type: 'string',
			default: 'btn-red',
		},
		randomString: {
			type: 'string',
			source: 'attribute',
			selector: 'span.btn',
			attribute: 'data-form',
		},
		media: {
			type: 'object',
		},
		textHeading: {
			type: 'boolean',
			default: true,
		},
		modalThanksImage: {
			type: 'object',
		},
		modalThanksHeading: {
			type: 'string',
			default: 'Thank you for getting in touch!',
		},
		modalThanksPar: {
			type: 'string',
			default: 'We\'ll get back to you within one business day!',
		},
		modalIntroHeading: {
			type: 'string',
			default: 'Tell us about your project',
		},
		modalIntroPar: {
			type: 'string',
			default: 'The more we know, the more accurate our estimate is!',
		},
	},
	supports: {
		anchor: true,
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: {
				headingImage,
				heading,
				description,
				blockStyle,
				actionButtonLabel,
				contactFormId,
				media,
				textHeading,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
				modalIntroHeading,
				modalIntroPar,
			},
			setAttributes,
		} = props;
		const updateAttribute = ( key, value ) => {
			setAttributes( {
				[key]: value,
			} );
		}

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody title={ __( 'Background color', 'qarea' ) }>
						<PanelRow>
							<ColorPalette // Element Tag for Gutenberg standard colour selector
								onChange={ val => {
									setAttributes( {
										blockStyle: {
											backgroundColor: val,
										}
									} )
								}
								}
							/>
						</PanelRow>
					</PanelBody>
					<FormDropdownControl
						// Thank you window
						media={ modalThanksImage }
						setModalImage={ ( media ) => {
							setAttributes({ modalThanksImage: media });
						} }
						removeModalImage={ () => setAttributes({ modalThanksImage: '' }) }
						modalThanksHeading={ modalThanksHeading }
						modalThanksHeadingEdit={ val => updateAttribute( 'modalThanksHeading', val ) }
						modalThanksPar={ modalThanksPar }
						modalThanksParEdit={ val => updateAttribute( 'modalThanksPar', val ) }
						// Form modal
						modalIntroHeading={ modalIntroHeading }
						modalIntroEdit={ val => updateAttribute( 'modalIntroHeading', val ) }
						modalIntroPar={ modalIntroPar }
						modalIntroParEdit={ val => updateAttribute( 'modalIntroPar', val ) }
						// Select parameters
						query={{
							per_page : -1, // set -1 to display ALL
							orderby : 'date',
							order : 'asc',
						}}
						setModalId={ ( randomString, content ) => {
							setAttributes({
								contactFormId: content,
								randomString: randomString + '-' + content,
							});
						} }
					/>
				</InspectorControls>
				<div
					className={ className }
					style={ blockStyle }
				>
					<div className="col">
						<IntroH1
							// Toggle image or text heading
							headingOption={ textHeading }
							onToggleChange={ val => updateAttribute( 'textHeading', val ) }
							// Process image
							media={ headingImage }
							setHeadingImage={ ( media ) => {
								setAttributes({ headingImage: media });
							} }
							// Process text
							removeHeadingImage={ () => setAttributes({ headingImage: '' }) }
							heading={ heading }
							text={ description }
							onHeadingChange={ val => updateAttribute( 'heading', val ) }
							onTextChange={ val => updateAttribute( 'description', val ) }
						/>
						<ButtonWithAction
							text={ actionButtonLabel }
							onButtonLabelChange={ val => updateAttribute( 'actionButtonLabel', val ) }
							formId={ contactFormId }
						/>
					</div>
					<div className="col">
						<ImageUpload
							media={ media }
							onImageSelect={ ( media ) => {
								setAttributes({ media: media });
							} }
							onImageRemove={ () => setAttributes({ media: '' }) }
						/>
					</div>
				</div>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			className,
			attributes: {
				headingImage,
				heading,
				description,
				blockStyle,
				actionButtonLabel,
				contactFormId,
				media,
				randomString,
				btnClass,
				textHeading,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
				modalIntroHeading,
				modalIntroPar,
			}
		} = props;

		return (
			<div
				className={ className + ' top-bg' }
				id="top-bg"
				style={ blockStyle }
			>
				<div className="section-banner">
					<section className="container">
						<div className="banner-info">
							<IntroH1.View
								headingOption={ textHeading }
								headingImage={ headingImage }
								heading={ heading }
								text={ description }
							/>
							<ButtonWithAction.View
								text={ actionButtonLabel }
								formId={ contactFormId }
								btnClass={ btnClass }
								modalId={ randomString }
								modalIntroHeading={ modalIntroHeading }
								modalIntroPar={ modalIntroPar }
								modalThanksImage={ modalThanksImage }
								modalThanksHeading={ modalThanksHeading }
								modalThanksPar={ modalThanksPar }
							/>
						</div>
						<div className="banner-animation">
							<img src={ media ? media.url : '' } alt="" />
						</div>
					</section>
					<section className="awards">
						<div className="container">
							<ul className="awards-list">
								<li><img src="https://qarea.com/wp-content/themes/qarea/assets3/img/awards-01.png" alt="ISO 27001:2013 Certified"/></li>
								<li><img src="https://qarea.com/wp-content/themes/qarea/assets3/img/awards-02.png" alt="CMMI-DEV Maturity Level 3"/></li>
								<li><img src="https://qarea.com/wp-content/themes/qarea/assets3/img/awards-03.png" alt="International Software Qualifications Board"/></li>
							</ul>
						</div>
					</section>
				</div>
			</div>
		);
	},
} );
