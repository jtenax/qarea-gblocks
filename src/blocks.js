/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './action-button';
import './block';
import './cta-row';
import './quick';
import './icon-accordion';
import './image-background';
import './inner-navigation';
// import './three-columns';
import './top-banner';
import './vertical-carousel';
