/**
 * BLOCK: action-link
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import classnames from 'classnames';
import FormDropdownControl from "../components/form-select"; '../components/form-select';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InspectorControls, ColorPalette } = wp.blockEditor;
const { PanelBody, PanelRow } = wp.components;
const { RawHTML, Fragment } = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-action-link', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'action-link - CGB Block' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'action', 'qarea' ),
		__( 'button', 'qarea' ),
		__( 'qarea', 'qarea' ),
	],
	attributes: {
		actionBtnLabel: {
			type: 'string',
		},
		contactFormId: {
			type: 'string',
		},
		randomString: {
			type: 'string',
			source: 'attribute',
			selector: '.btn-modal-open',
			attribute: 'data-form',
		},
		// Modal window parameters
		modalIntroHeading: {
			type: 'string',
		},
		modalIntroPar: {
			type: 'string',
		},
		modalThanksImage: {
			type: 'object',
		},
		modalThanksHeading: {
			type: 'string',
		},
		modalThanksPar: {
			type: 'string',
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const {
			className,
			attributes: {
				actionBtnLabel,
				contactFormId,
				modalIntroHeading,
				modalIntroPar,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
			},
			setAttributes,
		} = props;

		const updateAttribute = ( key, value ) => {
			setAttributes( {
				[key]: value,
			} );
		}

		// Creates a <p class='wp-block-cgb-block-action-link'></p>.
		return [
			<InspectorControls>
				<FormDropdownControl
					// Form modal
					modalIntroHeading={ modalIntroHeading }
					modalIntroEdit={ val => updateAttribute( 'modalIntroHeading', val ) }
					modalIntroPar={ modalIntroPar }
					modalIntroParEdit={ val => updateAttribute( 'modalIntroPar', val ) }
					// Thank you window
					media={ modalThanksImage }
					setModalImage={ ( media ) => {
						setAttributes({ modalThanksImage: media })
					} }
					removeModalImage={ () => setAttributes({ modalThanksImage: '' }) }
					modalThanksHeading={ modalThanksHeading }
					modalThanksHeadingEdit={ val => updateAttribute( 'modalThanksHeading', val ) }
					modalThanksPar={ modalThanksPar }
					modalThanksParEdit={ val => updateAttribute( 'modalThanksPar', val ) }
					// Select parameters
					query={{
						per_page : -1, // set -1 to display ALL
						orderby : 'date',
						order : 'asc',
					}}
					setModalId={ ( randomString, content ) => {
						setAttributes({
							contactFormId: content,
							randomString: randomString + '-' + content,
						});
					} }
				/>
			</InspectorControls>,
			<Fragment>
				<RichText
					tagName="span"
					className={ className + ' action-btn-label' }
					value={ actionBtnLabel }
					placeholder={ __( 'Button label' ) }
					keepPlaceholderOnFocus={ true }
					onChange={ val => updateAttribute( 'actionBtnLabel', val ) }
				/>
				<RawHTML>{ '[contact-form-7 id="' + contactFormId + '"]' }</RawHTML>
			</Fragment>
		]
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const {
			className,
			attributes: {
				actionBtnLabel,
				contactFormId,
				randomString,
				modalIntroHeading,
				modalIntroPar,
				modalThanksImage,
				modalThanksHeading,
				modalThanksPar,
			},

		} = props;

		return (
			<div
				className={ className + ' btn-modal-open'}
				data-form={ randomString }
			>
				<RichText.Content
					tagName="span"
					className="action-btn-label"
					value={ actionBtnLabel }
				/>
				<div
					style="display: none;"
					id={ randomString }
					className="t-center"
				>
					<h4 className="t-center">{ modalIntroHeading }</h4>
					<p>{ modalIntroPar }</p>
					<RawHTML>{ '[contact-form-7 id="' + contactFormId + '"]' }</RawHTML>
				</div>
				<div
					style="display: none;"
					className="form-modal t-center"
					id={ 'thank-' + randomString }
				>
					<img
						src={ modalThanksImage ? modalThanksImage.url : '' }
						alt="Animated Letter"
						className="img-center mb-50"
					/>
					<h4>{ modalThanksHeading }</h4>
					<p><RawHTML>{ modalThanksPar }</RawHTML></p>
				</div>
			</div>
		)
	},
} );
