const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { InspectorControls, InnerBlocks, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { PanelBody, Button, ResponsiveWrapper, Spinner, RangeControl } = wp.components;
const { compose, withState } = wp.compose;
const { withSelect } = wp.data;

const ALLOWED_MEDIA_TYPES = [ 'image' ];

class ImageSelectorEdit extends Component {
	render() {
		const { attributes, setAttributes, bgImage, className } = this.props;
		const { bgImageId, bgImageStyles } = attributes;
		const instructions = <p>{ __( 'To edit the background image, you need permission to upload media.', 'qarea' ) }</p>;

		let styles = {};
		if ( bgImage && bgImage.source_url ) {
			styles = { backgroundImage: `url(${ bgImage.source_url })` };
		}

		const onUpdateImage = ( image ) => {
			setAttributes( {
				bgImageId: image.id,
				bgImageStyles: { backgroundImage: `url(${ image.url })` }
			} );
		};

		const onRemoveImage = () => {
			setAttributes( {
				bgImageId: undefined,
				bgImageStyles: { backgroundImage: 'none' }
			} );
		};

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title={ __( 'Background settings', 'qarea' ) }
						initialOpen={ true }
					>
						<div className="wp-block-image-background">
							<MediaUploadCheck fallback={ instructions }>
								<MediaUpload
									title={ __( 'Background image', 'qarea' ) }
									onSelect={ onUpdateImage }
									allowedTypes={ ALLOWED_MEDIA_TYPES }
									value={ bgImageId }
									render={ ( { open } ) => (
										<Button
											className={ ! bgImageId ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview' }
											onClick={ open }>
											{ ! bgImageId && ( __( 'Set background image', 'qarea' ) ) }
											{ !! bgImageId && ! bgImage && <Spinner /> }
											{ !! bgImageId && bgImage &&
											<ResponsiveWrapper
												naturalWidth={ bgImage.media_details.width }
												naturalHeight={ bgImage.media_details.height }
											>
												<img src={ bgImage.source_url } alt={ __( 'Background image', 'qarea' ) } />
											</ResponsiveWrapper>
											}
										</Button>
									) }
								/>
							</MediaUploadCheck>
							{ !! bgImageId && bgImage &&
							<MediaUploadCheck>
								<MediaUpload
									title={ __( 'Background image', 'qarea' ) }
									onSelect={ onUpdateImage }
									allowedTypes={ ALLOWED_MEDIA_TYPES }
									value={ bgImageId }
									render={ ( { open } ) => (
										<Button onClick={ open } isDefault isLarge>
											{ __( 'Replace background image', 'qarea' ) }
										</Button>
									) }
								/>
							</MediaUploadCheck>
							}
							{ !! bgImageId &&
							<MediaUploadCheck>
								<Button onClick={ onRemoveImage } isLink isDestructive>
									{ __( 'Remove background image', 'qarea' ) }
								</Button>
							</MediaUploadCheck>
							}
						</div>
					</PanelBody>
				</InspectorControls>
				<div
					className={ className }
					style={ styles }
				>
					<InnerBlocks />
				</div>
			</Fragment>
		);
	}
}

export default compose(
	withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { bgImageId, bgImageStyles } = props.attributes;

		return {
			bgImage: bgImageId ? getMedia( bgImageId ) : null,
		};
	} ),
)( ImageSelectorEdit );
